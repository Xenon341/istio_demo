package net.abcdefab.entrypoint;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class Hello3Application {
	
	Logger logger = LoggerFactory.getLogger(Hello3Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Hello3Application.class, args);
	}
	
	@GetMapping(path = "/hello")
	public String hello() {
		return "Hello from Workload 3";
	}
	
	@GetMapping(path = "/private")
	public String privateMethod() {
		return "Private data of Workload 3";
	}
	
	@GetMapping(path = "/start")
	public String start(@RequestHeader Map<String, String> headers) {
		headers.forEach((key, value) -> {
			logger.info(String.format("Header '%s' = %s", key, value));
	    });
	    return "end reached on Workload 3";
	}

}
