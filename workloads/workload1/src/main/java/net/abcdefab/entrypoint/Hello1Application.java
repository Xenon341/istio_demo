package net.abcdefab.entrypoint;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@SpringBootApplication
public class Hello1Application {
	
	@Value("${config.string}")
	private String configString;
	
	Logger logger = LoggerFactory.getLogger(Hello1Application.class);
	
	private final static String[] headers_to_proagate = {"x-request-id","x-b3-traceid","x-b3-spanid","x-b3-sampled","x-b3-flags",
		      "x-ot-span-context","x-datadog-trace-id","x-datadog-parent-id","x-datadog-sampled", "end-user","user-agent"};

	public static void main(String[] args) {
		SpringApplication.run(Hello1Application.class, args);
	}
	
	@GetMapping(path = "/hello")
	public String hello() {
		return "Hello from Workload 1 avec fonction en test utilisateur";
	}
	
	@GetMapping(path = "/private")
	public String privateMethod() {
		return "Private data of Workload 1";
	}
	
	@GetMapping(path = "/config")
	public String config() {
		return configString;
	}
	
	@GetMapping(path = "/startChainCall")
	public String callMod2(@RequestHeader Map<String, String> headers) {
		headers.forEach((key, value) -> {
			logger.info(String.format("Header '%s' = %s", key, value));
	    });
		
		HttpHeaders newReqHeaders = new HttpHeaders();
		
		for (String header : headers_to_proagate) {
	        String value = headers.get(header);
	        if (value != null) {
	        	newReqHeaders.add(header,value);
	        }
	      }
		
		RestTemplate restTemplate = new RestTemplate();
		final String uri = "http://workload2-service:8080/start";
		
		HttpEntity<String> entity = new HttpEntity<String>("parameters", newReqHeaders);
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

	    
	    //String result = restTemplate.getForObject(uri, String.class);
	    return result.getBody();
	}
	
	@GetMapping(path = "/callSleep")
	public String callSleep(@RequestHeader Map<String, String> headers) {
		headers.forEach((key, value) -> {
			logger.info(String.format("Header '%s' = %s", key, value));
	    });
		
		HttpHeaders newReqHeaders = new HttpHeaders();
		
		for (String header : headers_to_proagate) {
	        String value = headers.get(header);
	        if (value != null) {
	        	newReqHeaders.add(header,value);
	        }
	      }
		
		RestTemplate restTemplate = new RestTemplate();
		final String uri = "http://workload2-service:8080/sleep";
		
		HttpEntity<String> entity = new HttpEntity<String>("parameters", newReqHeaders);
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

	    
	    //String result = restTemplate.getForObject(uri, String.class);
	    return result.getBody();
	}
	
	@GetMapping(path = "/callError")
	public String callError(@RequestHeader Map<String, String> headers) {
		headers.forEach((key, value) -> {
			logger.info(String.format("Header '%s' = %s", key, value));
	    });
		
		HttpHeaders newReqHeaders = new HttpHeaders();
		
		for (String header : headers_to_proagate) {
	        String value = headers.get(header);
	        if (value != null) {
	        	newReqHeaders.add(header,value);
	        }
	      }
		
		RestTemplate restTemplate = new RestTemplate();
		final String uri = "http://workload2-service:8080/error";
		
		HttpEntity<String> entity = new HttpEntity<String>("parameters", newReqHeaders);
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

	    
	    //String result = restTemplate.getForObject(uri, String.class);
	    return result.getBody();
	}

}
